-- author : stdgregwar
-- Curves library for Solarus
-- ease the making of animations

-- Curves example used

-- -- TODO


-- Animator example use
-- -- create or get a sprite or drawable
-- local title = sol.sprite.create("menus/solarus_logo")
-- title:set_animation("sun")

-- -- create an animator for the property 'xy' (multi-channel)
-- -- animator constructor takes a table of parameters
-- -- {[1] or props = <proplist>} : list of properties to animate
-- -- {duration : number} : duration of the animation
-- -- {loop : bool} : loop flag should animator loop?

-- -- <proplist> has the form {prop_name1=<prop_anim>,prop_name_2=<prop_anim>}
-- -- <prop_anim> has the form {[1]/curve=string|{string*},to=elem|{elem*}, from=elem|{elem*}, [stretch=1.0]
-- -- to and from size should match, size of curve can be shorter and the first curve is used as default,
-- -- note that multi channel to/from implies an array of curves, even if array as a single element
-- -- stretch is used to multiply the time when you pass a function

-- -- curve can be a string designating an easing shape (easing table) or directly a fonction [0,1] -> [0,1] (function can also be a callable object)
-- local animator = curves:animator{{
--     xy={{'in_lin','in_bounce'},to={220,200}, from={0,0}},
--                                  },
--   duration=1000}

-- -- create an animation (a running animator)
-- -- take a table of parameters
-- -- {[1]/object} object to animate properties on
-- -- {rate} rate of update, defaults to 60/s
-- -- {duration} override animator duration
-- -- {loop} overrride animator loop flag
-- -- {on_finished} an optional end callback (not called when looping)
-- local animation = animator:start{
--   title,
--   rate = 100,
--   duration=500,
--   loop = true,
--   on_finished= function() print('inline finished') end
-- }

-- -- other way to add the on_finished event, overrides the first one
-- function animation:on_finished()
--   print("FINISHED")
-- end


local curve_mt = {}

--check if given arg is a curve
local function assert_curve(c)
  assert(getmetatable(c) == curve_mt, "expected curve")
end

-- takes two tables and zip them together as multiple returns
local function zipt(t1,t2)
  for _,v in ipairs(t2) do
    t1[#t1+1] = v
  end
  return unpack(t1)
end

-- make a new curve from a function, a duration and a loop flag
function curve_mt:new(fun, duration, loop)
  duration = duration or 1.0
  local nc = {duration=duration, loop=loop}
  function nc:apply(t)
    return fun(t)
  end
  return setmetatable(nc, curve_mt)
end

-- calling the curve evaluate it
function curve_mt:__call(...)
  return self:apply(...)
end

-- slicing take a subpart of the curve
function curve_mt:slice(from,to)
  return self:new(
    function(t)
      return self(t-from)
    end,
    to-from)
end

-- Concatenation paste the two curves together
function curve_mt:__concat(other)
  assert_curve(other)
  return self:new(
    function(t)
      if t > self.duration then
        return other(t-self.duration)
      else
        return self(t)
      end
  end,
  self.duration+other.duration,
  other.loop)
end

-- Multiplying two curves zip them
function curve_mt:__mul(other)
  assert_curve(other)
  return self:new(
    function(t)
      return zipt({self(t)},{other(t)})
    end,
    math.max(self.duration,other.duration) --TODO: check if duration is ~ correct
  )
end

-- return the same curve with a different duration
function curve_mt:with_duration(duration)
  local factor = self.duration / duration
  return self:new(
    function(t)
      return self(factor*t)
    end,
  duration,
  self.loop)
end

--============================
-- primitives curves types
--============================

local prim_ease = {}
function prim_ease.lin(t)
  return t
end

function prim_ease.quad(t)
  return t*t
end

function prim_ease.cubic(t)
  return t*t*t
end

function prim_ease.quart(t)
  return t*t*t*t
end

function prim_ease.quint(t)
  return t*t*t*t*t
end

function prim_ease.sine(t)
  return 1-math.cos(t*math.pi*0.5)
end

function prim_ease.expo(t)
  return math.pow(2,10*(t-1))
end

function prim_ease.circ(t)
  local t = t-2
  return math.sqrt(1 - t*t)
end

function prim_ease.elastic(t)
  local s = 1.70158
  local p = 0.3
  if t == 0 then return 0 end
  local t = t-1
  return (math.pow(2,10*t) * math.sin((t-s)*(2*math.pi)/p))
end

function prim_ease.back(t)
  local s = 1.70158
  return t*t*((s+1)*t-s)
end

function prim_ease.bounce(t)
  if t < 1/2.75 then
		return (7.5625*t*t) ;
  elseif t < 2/2.75 then
    local t = t - (1.5/2.75) 
		return (7.5625*t*t + .75);
  elseif t < 2.5/2.75 then
    local t = t - (2.25/2.75)
    return (7.5625*t*t + .9375);
  else
    local t = t - (2.625/2.75)
    return (7.5625*t*t+ .984375);
  end
end

--
local ease = {}
for k,v in pairs(prim_ease) do
  local _in = v
  local function out(t)
    return 1-v(1-t)
  end
  local function inout(t)
    if t < 0.5 then
      return _in(t*2)*0.5
    else
      return 0.5+out((t-0.5)*2)*0.5
    end
  end
  ease['in_'..k] = _in
  ease['out_'..k] = out
  ease['in_out_'..k] = inout
end

curve_mt.fun = ease

-- create a new easing smart curve
function curve_mt:ease(params)
  local curve = params[1] or params.curve or prim_ease.lin
  local from = params[2] or params.from or 0
  local to = params[3] or params.to or 1
  local duration = params[4] or params.duration or 1
  local loop = params[5] or params.loop
  if type(curve) == 'string' then
    curve = assert(ease[curve], 'no curve named ' .. curve)
  end
  
  local extent = to-from
  return self:new(
    function(t)
      return from + curve(t/duration) * extent
    end,
    duration,
    loop
    )
end


local function check_easing(easing)
  if easing and type(easing) == 'string' then
    return assert(ease[easing], 'no curve named ' .. easing)
  end
  -- TODO add checks for callability 
  return easing
end
  
-- duplicate a table
local function shallow_copy(table)
  local t = {}
  for k,v in pairs(table) do
    t[k] = v
  end
  return t
end

local animator_mt = {}
animator_mt.__index = animator_mt

-- return the number of channels of a property
local function is_multi_channel_prop(prop)
  local vals = prop.to or prop.from
  if type(vals) == 'table' then -- multiple values
    return true, #vals
  end
  return false, 1 --single channel prop
end
  

-- create a new animator that can be used multiple times
function curve_mt:animator(params)
  assert(type(params) == 'table', "arguments expected as table")
  local props = params[1] or params.params
  local duration = params[2] or params.duration
  
  local resolved_props = {}
  
  for name,p in pairs(props) do
    
    local multi, width = is_multi_channel_prop(p)
    local rp = shallow_copy(p)
    rp.width = width -- add width attribute to resolved props
    rp.multi = multi
    local easing = p[1] or p.curve
    
    if not multi then
      rp.curve = check_easing(easing)
    else
      assert(type(easing) == 'table', "multiple channel easing type should ba a table of easing types")
      local curves = {}
      for i = 1, width do
        curves[i] = check_easing(easing[i] or easing[1]) -- duplicate easing if solo for multiple channels
      end
      rp.curve = curves
    end
    
    resolved_props[name] = rp
  end
  
  return setmetatable({
      props=resolved_props,
      duration=duration},
      animator_mt)
end

-- get current value of a property and a corresponding setter
local function current_prop(object, prop_name)
  local getter = object['get_' .. prop_name]
  local setter = object['set_' .. prop_name]
  if getter then
    if not setter then
      error("object has getter for property " .. prop_name .. " but no setter")
    end
    local values = {getter(object)}
    if #values == 1 then
      return values[1], setter
    else
      return values, setter
    end
  else
    local value = object[prop_name]
    if type(value) == 'table' then
      local setter = function(object,...)
        local vals = {...}
        object[prop_name] = vals
      end
      return value, setter
    else
      local setter = function(object, val)
        object[prop_name] = val
      end
      return value, setter
    end
  end
end

local animation_mt = {}
animation_mt.__index = animation_mt

-- start the animation on an arbitrary object with getter/setters or properties
function animator_mt:start(params)
  assert(type(params) == 'table', "arguments expected as table")
  local object = assert(params[1] or params.object, "missing object to animate")
  local context = params[2] or params.context
  local on_finished = params[3] or params.on_finished or function() end --on_finished always exists
  local delay = 1000 / (params.rate or 60)
  
  local props = self.props
  local updators = {} -- updators : (object, factor) -> unit
  
  for prop_name, prop in pairs(props) do
    local value, setter = current_prop(object, prop_name)
    local from = prop.from or value -- sets current value as default if not provided
    local to = prop.to or value
    local curve = prop.curve
    local updator
    if not prop.multi then --simple case of one scalar value
      local extent = to - from
      updator = function(object, factor)
        factor = factor*(prop.stretch or 1)
        local val = from + extent*curve(factor)
        setter(object,val)
      end
    else -- construct multiple values to call the multi-channel setter
      updator = function(object, factor)
        factor = factor*(prop.stretch or 1)
        local vals = {n=prop.width}
        for i = 1,prop.width do
          local fromi = from[i]
          local faci = curve[i](factor)
          vals[i] = fromi + (to[i] - fromi) * faci
        end
        setter(object, unpack(vals))
      end
    end
    updators[prop_name] = updator
  end
  
  
  --create animation object
  local animation = setmetatable({
      on_finished = on_finished,
      object = object,
      context = context,
      delay=delay,
      duration = self.duration, -- duration of the animation is the duration of the animator
      time = 0,
      updators=updators
      }, animation_mt)
  
  
  local function update_anim()
    return animation:update()
  end
  
  --start the timer with the context, if given
  local timer = context and
    sol.timer.start(context,delay,update_anim) or
    sol.timer.start(delay,update_anim)
  
  animation.timer = timer
  return animation
end

-- update the animation, (must be called regularly by a timer), return true to keep timer alive
function animation_mt:update()
  self.time = self.time + self.delay
  
  local factor = math.min(self.time / self.duration, 1)
  for _,updator in pairs(self.updators) do
    updator(self.object, factor)
  end
  
  if self.time >= self.duration then
    self:on_finished() -- only call finished if we actually finished
    return false
  end
  
  return true --continue as long as we are not past duration
end

function animation_mt:stop()
  self.timer:stop()
end

return curve_mt
